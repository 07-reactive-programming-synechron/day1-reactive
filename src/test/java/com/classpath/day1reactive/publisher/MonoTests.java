package com.classpath.day1reactive.publisher;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class MonoTests {

    @Test
    public void testMonoPublisher(){
        Mono<String> emptyMono = Mono.empty();

        StepVerifier.create(emptyMono).expectComplete();
    }

    @Test
    public void testSingleMonoPublisher(){
        Mono<String> stringMono = Mono.just("Reactive-Programming");

        StepVerifier.create(stringMono).expectNext("Reactive-Programming").expectComplete();
    }

    @Test
    public void testSubscribeMethod(){
        Flux<String> stringFlux = Flux.just("one", "two", "three"). concatWith(Mono.error(new RuntimeException("invalid data")));

        stringFlux.subscribe((data) -> System.out.println(data), (error)-> System.out.println(" Error " + error.getMessage()), ()-> System.out.println("completed"));
    }
}