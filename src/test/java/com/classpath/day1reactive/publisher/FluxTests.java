package com.classpath.day1reactive.publisher;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class FluxTests {

    @Test
    public void testProductConstructor(){
        Product product = Product.builder().date(LocalDate.now()).name("Camlin").price(23).build();
        assertNotNull(product);
        assertEquals(23, product.getPrice());
        assertEquals("Camlin", product.getName());
    }

    @Test
    public void testMapFunction() {
        Flux<String> fluxOfString = Flux.fromArray(new String[]{"one", "two", "three", "four"});
        Flux<Integer> integerFlux = fluxOfString.map(String::length);
        //integerFlux.subscribe(data -> System.out.println(data));
        StepVerifier
                .create(integerFlux)
                .expectNext(3, 3, 5,4)
                .expectComplete()
                .verify();
    }
}