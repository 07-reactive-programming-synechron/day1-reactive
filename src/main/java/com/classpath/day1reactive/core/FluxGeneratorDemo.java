package com.classpath.day1reactive.core;

import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FluxGeneratorDemo {

    public static void main(String[] args) {
        Flux.range(2, 10).subscribe(System.out::println);
        Flux.fromIterable(Arrays.asList(1,2,3,4)).filter(x -> x % 2 == 0).subscribe(System.out::println);
        Integer [] array = {1,2,3,4};
        Flux.fromArray(array).filter(x -> x % 2 == 0).subscribe(data -> Collectors.toList());
        Flux.just(22,3,44,55);
        Flux.fromStream(Stream.of(1,2,3,4));
        Flux.empty();
    }
}