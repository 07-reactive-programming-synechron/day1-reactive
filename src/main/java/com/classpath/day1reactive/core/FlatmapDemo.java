package com.classpath.day1reactive.core;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import reactor.core.publisher.Flux;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public class FlatmapDemo {
    public static void main(String[] args) {

        /*
            Flux<Integer[]> => flatmap => Flux<Integer>
            Flux<List<String>> => flatmap => Flux<String>
            Flux<Set<String>> => flatmap => Flux<String>
            Flux<Stream<String>> => flatmap => Flux<String>
            Flux<Flux<String>> => flatmap => Flux<String>
            Flux<String> => flatmap => Flux<String>
         */
        Flux<Order> fluxOfOrders = fetchAllOrders();
        Flux<Set<LineItem>> fluxOfSetOfLineItems = fluxOfOrders.map(Order::getLineItems);
        Flux<LineItem> fluxOfLineItems = fluxOfSetOfLineItems.flatMap(Flux::fromIterable);
        fluxOfLineItems.distinct().subscribe(lineItem -> System.out.println(lineItem.getName()));
        /*
                Flux<Double> doubleFlux = fluxOfLineItems.map(LineItem::getPrice);
                Optional<Double> max = doubleFlux.toStream().max((p1 , p2) -> ((int)(p1 - p2)));
        */
        double max = fluxOfLineItems.toStream().mapToDouble(LineItem::getPrice).max().getAsDouble();
        System.out.println(" Max value is "+ max);
        fluxOfLineItems.count().subscribe(System.out::println);
    }

    private static Flux<Order> fetchAllOrders() {
        Faker faker = new Faker();
        Set<Order> orders = new HashSet<>();
        IntStream.range(1, 10)
                .forEach(index -> {
                    Order order = Order.builder()
                            .customerEmail(faker.internet().emailAddress())
                            .id(faker.number().randomNumber())
                            .lineItems(new HashSet<>())
                            .build();
                    IntStream.range(1,4).forEach(val -> {
                        LineItem lineItem = LineItem.builder()
                                .name(faker.commerce().productName())
                                .price(faker.number().randomDouble(2, 10000, 20000))
                                .qty(faker.number().numberBetween(2,5))
                                .build();
                        order.getLineItems().add(lineItem);
                    });
                    orders.add(order);
                });
        return Flux.fromIterable(orders);
    }
}


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class Order {
    private long id;
    private String customerEmail;
    private Set<LineItem> lineItems;
}


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class LineItem {
    private double price;
    private String name;
    private int qty;
}