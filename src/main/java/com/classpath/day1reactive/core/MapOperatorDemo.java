package com.classpath.day1reactive.core;

import com.classpath.day1reactive.publisher.Product;
import com.github.javafaker.Faker;
import reactor.core.publisher.Flux;

import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;

public class MapOperatorDemo {

    public static void main(String[] args) {

        Flux<Product> productFlux = fetchAllProducts();
        Predicate<Product> productPriceLessThan40k  = (product) -> product.getPrice() < 40_000;
        Function<Product, String> mapProductToName = Product::getName;

        Flux<Product> productsFilteredByPriceLessThan40k = productFlux.filter(productPriceLessThan40k);
        Flux<String> productsNameFilteredByPriceLessThan40k = productsFilteredByPriceLessThan40k.map(mapProductToName);

        //productsNameFilteredByPriceLessThan40k.subscribe(productName -> System.out.println("Name of the product :: "+ productName));

        productFlux
                .filter(product -> product.getPrice() < 35_000)
                .subscribe(product -> System.out.println("Product Name :"+ product.getName() + ","+ " Product Price :: "+ product.getPrice()));

    }

    private static Flux<Product> fetchAllProducts() {
        Faker faker = new Faker();
        Set<Product> products = new HashSet<>();
        for(int i = 0; i < 10; i ++){
            Product product = Product.builder()
                    .id(faker.random().nextLong())
                    .date(faker.date().past(10, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                    .name(faker.commerce().productName())
                    .price(faker.number().randomDouble(2, 25_000, 40_000))
                    .build();
            products.add(product);
        }
        return Flux.fromIterable(products);
    }
}