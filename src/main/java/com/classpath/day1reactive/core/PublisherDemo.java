package com.classpath.day1reactive.core;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class PublisherDemo {

    public static void main(String[] args) {
        Mono<Integer> integerMono = Mono.just(22);
        Flux<Integer> integerFlux = Flux.just(11, 22, 33, 23, 5, 66, 77);

        //integerMono.subscribe(data -> System.out.println(data));

       // integerFlux.subscribe(data -> System.out.println(data));
/*

        integerFlux.subscribe(new Subscriber<Integer>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(Integer.MAX_VALUE);

            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("On next method called :: "+ integer);
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(" Exception from the publisherd "+ t.getMessage());
            }

            @Override
            public void onComplete() {
                System.out.println("Publisher has completed the values. No more data from the producer");
            }
        });
*/

/*
        integerMono.subscribe(new Subscriber<Integer>() {
            @Override
            public void onSubscribe(Subscription s) {
                s.request(10);
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("Value "+integer);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                System.out.println("Subscription ended");
            }
        });
*/

/*
        integerFlux.subscribe(new Subscriber<Integer>() {
            private Subscription subscription;
            int counter;
            @Override
            public void onSubscribe(Subscription subscription) {
                this.subscription = subscription;
                subscription.request(2);
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println(" Number "+ integer);
                counter++;
                if(counter % 2 == 0){
                    System.out.println(":::: Requesting for two more records :::: ");
                    subscription.request(2);
                }

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                System.out.println(":::: No more records to consume ::::");
            }
        });
        */

        //subscription cancel demo..
        integerFlux.subscribe(new Subscriber<Integer>() {
            private Subscription subscription;
            int counter;
            @Override
            public void onSubscribe(Subscription subscription) {
                this.subscription = subscription;
                subscription.request(2);
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println(" Number "+ integer);
                if (integer == 23){
                    subscription.cancel();
                }
                counter++;
                if(counter % 2 == 0){
                    System.out.println(":::: Requesting for two more records :::: ");
                    subscription.request(2);
                }

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {
                System.out.println(":::: No more records to consume ::::");
            }
        });
    }
}