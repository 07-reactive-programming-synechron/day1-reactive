package com.classpath.day1reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day1ReactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day1ReactiveApplication.class, args);
    }

}
